import { TestBed } from '@angular/core/testing';

import { BaseStore } from './base.store';

describe('BaseService', () => {
  type typeA = {};
  type typeB = {};
  let service: BaseStore<typeA, typeB>;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BaseStore);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
