import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayComponent } from './play.component';

describe('PlayComponent', () => {
  let component: PlayComponent;
  let fixture: ComponentFixture<PlayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change button color if it was clicked', () => {
    const btn: HTMLElement = fixture.debugElement.nativeElement.querySelector('.answer-button')
    const clickEvent = new Event('click');
    btn.dispatchEvent(clickEvent)
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(btn.getAttribute('class')).toContain("bg-secondary-default");
    });
  });

  it('should show error message to the user in case the request fails', () => {
    
  });
});
