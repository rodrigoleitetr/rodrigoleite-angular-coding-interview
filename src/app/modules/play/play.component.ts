import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../../core/services/questions.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { QuestionModel } from '../../core/state/question.model';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss']
})
export class PlayComponent implements OnInit {

  public correctAnswers: number = 0;
  public inCorrectAnswers: number = 0;
  public userCompletedGame: boolean = false;

  questions$ = this.questionsService.questions$;
  gettingQuestions$ = this.questionsService.gettingQuestions$;
  getQuestionsSubscription: Subscription = this.route.queryParams
    .pipe(switchMap(params =>
      this.questionsService.getQuestions({
        type: params.type,
        amount: params.amount,
        difficulty: params.difficulty
      })
    )).subscribe();


  constructor(
    private readonly route: ActivatedRoute,
    private readonly questionsService: QuestionsService,
  ) { }

  ngOnInit(): void {
    this.questions$.subscribe((questions: QuestionModel[]) => {
      console.log(questions);
        this.userCompletedGame = questions.every(x => x.selectedId !== undefined);
        if(this.userCompletedGame){
            this.correctAnswers = questions.filter(q => q.selectedId == q.answers.find(a => a.isCorrect)?._id).length;
            this.inCorrectAnswers =  questions.length - this.correctAnswers;
        }
    }, (error: any) => {
      alert('It was not possible ot get the questions.');
      console.log(error);
    });
  }

  onAnswerClicked(questionId: QuestionModel['_id'], answerSelected: string): void {
    this.questionsService.selectAnswer(questionId, answerSelected);
  }

}
